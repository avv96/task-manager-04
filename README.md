<a align="center" href="https://imgbb.com/"><img width=12.5% src="https://i.ibb.co/NWdBMFN/quote-2021-08-08-96cd3fd55d61368c98a20ffe5249c186.jpg" alt="quote-2021-08-08-96cd3fd55d61368c98a20ffe5249c186" border="0"></a>

# TASK MANAGER

## DEVELOPER INFO

* **NAME**: Vinokurov Aleksey

* **E-MAIL**: avv96@yandex.ru

## SOFTWARE

* OpenJDK 8 (v.1.8.0_221)

* IntelliJ IDEA 2019.3.5 (Ultimate Edition)

* Windows 10 (v.1809)

## HARDWARE

* **RAM**: 16Gb

* **CPU**: Intel(R) Core(TM) i5-8265U CPU @1.60GHz

* **SSD**: 512Gb

## RUN PROGRAM

```shell script
java -jar ./task-manager.jar
```
